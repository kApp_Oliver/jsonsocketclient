import Foundation
import Socket
import SSLService

public protocol JSONSocketClientDelegate {
    func received(message: JSONSocketClient.PayloadType)
    func clientDisconnected()
}

public class JSONSocketClient {
    public enum SocketType {
        case tcp
        case udp
    }
    public enum LogLevel {
        case none
        case verbose
    }
    public let id = UUID()
    public typealias PayloadType = [String : Any]
    public var socket : Socket?
    public var delegate : JSONSocketClientDelegate?
    public var logLevel : LogLevel = .none
    public var boundary : String?
    var messageQueue = DispatchQueue(label: "uk.kAppuccino.JSONSocketClient.messageQueue", qos: .background)
    var socketQueue = DispatchQueue(label: "uk.kAppuccino.JSONSocketClient.socketQueue", qos: .background)
    var buffer = Data()
    public var socketType : SocketType = .tcp
    var host : String = "127.0.0.1"
    var port : Int = 8080
    
    func log(_ message: String){
        if logLevel == .verbose {
            print("JSONSocketServerClient [\(id.uuidString)] > \(message)")
        }
    }
    
    public func close(){
        socket?.close()
        log("Socket Closed - \(id.uuidString)")
        delegate?.clientDisconnected()
    }
    
    public func send(payload: PayloadType,withBoundary : Bool = true){
        messageQueue.sync { [unowned self,socket] in
            do {
                guard try socket?.isReadableOrWritable().writable == true else {
                    return
                }
            }catch let e {
                print(e.localizedDescription)
                return
            }
            guard socket?.isConnected == true else {
                return
            }
            guard let data = try? JSONSerialization.data(withJSONObject: payload, options: .init()) else {
                return
            }
            guard socket?.isConnected == true else {
                return
            }
            if withBoundary && self.boundary != nil {
                let _ = try? socket?.write(from: self.boundary!)
            }
            let _ = try? socket?.write(from: data)
        }
    }
    func handleBuffer(){
        guard let bufferValue = String(data: buffer, encoding: .utf8) else {
            log("Invalid character in buffer.")
            close()
            return
        }
        var messages : [Data] = [buffer]
        if let boundary = boundary {
            let components = bufferValue.components(separatedBy: boundary)
            messages = []
            for component in components {
                guard let componentData = component.data(using: .utf8) else {
                    continue
                }
                messages.append(componentData)
            }
        }
        var validBuffer = false
        for messageData in  messages {
            if let payloadInstance = try? JSONSerialization.jsonObject(with: messageData, options: .allowFragments) as? PayloadType {
                if let newBoundary = payloadInstance["boundary"] as? String {
                    self.boundary = newBoundary
                    log("Boundary applied - \(newBoundary)")
                }
                delegate?.received(message: payloadInstance)
                validBuffer = true
            }
        }
        if validBuffer {
            buffer.removeAll()
        }
    }
    
    func handle(){
        guard socket?.isConnected == true else {
            log("Ending handle for closed socket.")
            return
        }
        do {
            let result = try socket?.read(into: &buffer)
            if result == 0 {
                close()
                return
            }
            handleBuffer()
            
        }catch let e {
            log("Error caught - \(e.localizedDescription)")
            close()
            return
        }
        socketQueue.async {
            autoreleasepool {
                self.handle()
            }
        }
    }
    
    public init(host: String,port: Int,delegate: JSONSocketClientDelegate? = nil,logLevel: LogLevel = .none,usingSSL: Bool = true,socketType: SocketType = .tcp) throws{
        self.delegate = delegate
        self.socketType = socketType
        self.host = host
        self.port = port
        if socketType == .tcp {
            self.socket = try Socket.create()
        }else {
            self.socket = try Socket.create(family: .inet, type: .datagram, proto: .udp)
            try self.socket?.udpBroadcast(enable: true)
        }
        if usingSSL {
            let sslService = try SSLService(usingConfiguration: SSLService.Configuration(withCipherSuite: "ALL", clientAllowsSelfSignedCertificates: true))
            sslService?.skipVerification = true
            self.socket?.delegate = sslService
        }
        try self.socket?.connect(to: host, port: Int32(port))
        socketQueue.async {
            autoreleasepool {
                if self.socketType == .udp {
                    //SEND HELLO...
                    self.send(payload: ["message" : "HELLO"])
                }
                self.handle()
            }
        }
    }
}
