import XCTest
@testable import JSONSocketClient

final class JSONSocketClientTests: XCTestCase, JSONSocketClientDelegate {
    func received(message: JSONSocketClient.PayloadType) {
        print(message)
        sleep(1)
        client?.send(payload: ["message" : "Hi!"])
    }
    
    func clientDisconnected() {
        print("Disconnected!")
    }
    var client : JSONSocketClient?
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        
        do {
            client = try JSONSocketClient(host: "127.0.0.1", port: 8080,delegate: self,logLevel: .verbose, usingSSL: false,socketType: .udp)
            while client?.socket?.isConnected == true {
                print("Keep Alive")
                client?.send(payload: ["message" : "Hello!"])
                sleep(1)
            }
            XCTAssert(true)
        }catch let e {
            print("Error - \(e.localizedDescription)")
            client?.close()
            client = nil
            XCTAssert(false)
        }
        
    }
    
    static var allTests = [
        ("testExample", testExample),
    ]
}
