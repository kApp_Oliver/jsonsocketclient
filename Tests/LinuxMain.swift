import XCTest

import JSONSocketClientTests

var tests = [XCTestCaseEntry]()
tests += JSONSocketClientTests.allTests()
XCTMain(tests)
